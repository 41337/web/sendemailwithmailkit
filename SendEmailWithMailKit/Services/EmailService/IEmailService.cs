﻿namespace SendEmailWithMailKit.Services.EmailService
{
    public interface IEmailService
    {

        void SendEmail(EmailDTO email);
    }
}